<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/mainUserEdit.css">
        <link rel="stylesheet" type="text/css" href="css/general.css">
        <link rel="shortcut icon" href="img/logo.png"/>
        <title>Edit Profile</title>
    </head>
    <body>
        <div id="principal">


            <h1>Username</h1>
            <img src="img/photoUsers.png" alt="" height="158" width="179">
            <br><br>
            <a href="">Change photo</a>
            <br><br>
            <form name="mainUserEdit" action="mainUserEdit.php">
                Name:
                <input type="text" name="name" value="">
                <br><br>
                Surname:
                <input type="text" name="lastname" value="">
                <br><br>
                Telephone:
                <input type="tel" name="telephone" value="">
                <br><br>
                Birthday:
                <input type="date" name="birthday" value="">
                <br><br>
                Email:
                <input type="email" name="email" value="">
                <br><br>
                Address:
                <input type="text" name="address" value="">
                <br><br>
            </form> 

            <a href="mainUser.php">
                <input type="button" value="Save">
            </a>

            <a href="mainUser.php">
                <input type="button" value="Cancel">
            </a>
        </div>
    </body> 
</html>