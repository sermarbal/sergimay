<meta charset="UTF-8">

<?php

    session_start();

?>
    <h1 align="center">Torna quan vulguis!</h1>
    <br><br>
    <h2 align="center">Variables de la sessió <?php echo session_id(); ?> amb nom <?php echo session_name(); ?> han sigut actualitzades.</h2>
    <br>
    <center><a href="../">Tornar a Login</a></center>

<?php
    session_destroy();
    setcookie(session_name(), '', time() - 42000,'/');
?>