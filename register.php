<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/register.css">
        <link rel="shortcut icon" href="img/logo.png"/>   
        <title>Register</title>
    </head>

    <body>
        <div id="principal">
        <form action="register.php" method="POST">
            <h1>Register</h1>
            <table>
                <tr>
                    <td>Username:</td>
                    <td><input type="text" name="userName"></td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" name="name"></td>
                    <td>Surname:</td>
                    <td><input type="text" name="name"></td>
                </tr>

                <tr>
                    <td>Password:</td>
                    <td><input type="password" name="password"></td>
                    <td>Repeat Password:</td>
                    <td><input type="password" name="password"></td>
                </tr>

                <tr>
                    <td>Birthday:</td>
                    <td><input type="tel" name="birthday"></td>
                    <td>Telephone:</td>
                    <td><input type="tel" name="telephone"></td>
                </tr>
                
                <tr><td><iframe src="http://www.w3schools.com"></iframe></td></tr>
                
                <tr>
                    <td>Email:</td>
                    <td><input type="email" name="email"></td>
                    <td>Address:</td>
                    <td><input type="text" name="address"></td>
                    
                </tr>

            </table>
            <br>
            <input type="submit" value="Register">
            <br>
            <a href="login.php">Return to login</a>


        </form>
    </div>
</body>
</html>
