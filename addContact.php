<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/addContact.css">
        <script type="text/javascript" src="js/functions.js"></script>
        <?php require 'php/functions.php'; ?>
        <link rel="shortcut icon" href="img/logo.png"/>
        <title>Add Contact</title>
    </head>

    <?php
    session_start();
    security();
    if (!$_POST) {
        ?>

        <body>
            <div id="principal">

                <div id="close">
                    <a href="Main.php">
                        <img src="img/popUpClose.png" alt="" height="38" width="38">
                    </a>
                </div>

                <div id="addContact">
                    <h1>Add contact</h1>
                    <img src="img/photoUsers.png" alt="" height="114" width="131">
                    <br>
                    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                        <label>Name:</label>
                        <input type="text" name="name" value="" class="info">
                        <br>
                        <label>Surname:</label>
                        <input type="text" name="surname" value="" class="info">
                        <br>
                        <label>Telephone:</label>
                        <input type="tel" name="telephone" value="" class="info">
                        <br>
                        <label>Birthday:</label>
                        <input id="dateInput" onclick="showCalendar()" type="date" name="birthday" value="yyyy-mm-dd" class="info">
                        <div align="center">
                            <iframe id="iframe" src="js/calendar.html" frameborder="0" scrolling="no" onload="resizeIframe(this)" >
                            </iframe>
                        </div>

                        <label>Email:</label>
                        <input type="email" name="email" value="" class="info">
                        <br>
                        <input type="submit" value="Save" class="button">
                        <div id="divider"/>
                    </form>
                    <form action="Main.php">
                        <input type="submit" value="Cancel" class="button">
                    </form>
                    
                    <?php if (isset($_COOKIE['error'])) { ?>
                        <script type="text/javascript">
                            errorContact(<?php echo $_COOKIE['error'] ?>);
                        </script>

                        <?php
                        setcookie('error');
                    }
                } else {

                    if ($_POST['name'] == null) {
                        setcookie("error", 0);
                        header('Location: ' . $_SERVER['PHP_SELF']);
                    } else if ($_POST['telephone'] == null) {
                        setcookie("error", 1);
                        header('Location: ' . $_SERVER['PHP_SELF']);
                    } else if ($_POST['birthday'] == null) {
                        setcookie("error", 2);
                        header('Location: ' . $_SERVER['PHP_SELF']);
                    } else if ($_POST['email'] == null) {
                        setcookie("error", 3);
                        header('Location: ' . $_SERVER['PHP_SELF']);
                    } else {

                        // Create connection
                        $conn = getConnection();
                        // Check connection
                        if ($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }

                        $addSurname = "";
                        $surname = "";

                        if ($_POST['surname']) {
                            $surname = "'" . $_POST['surname'] . "', ";
                            $addSurname = "surname, ";
                        }

                        $sql = "INSERT INTO CONTACTS (name, " . $addSurname . "telephone, birthday, email, id_user)
                            VALUES ('" . $_POST['name'] . "',  $surname" . "'" . $_POST['telephone'] . "', '" . $_POST['birthday'] . "', '" . $_POST['email'] . "', " . $_SESSION['id'] . ")";

                        if ($conn->query($sql) === TRUE) {
                            echo "New record created successfully";
                        } else {
                            echo "Error: " . $sql . "<br>" . $conn->error;
                        }
                        $conn->close();
                    }
                    ?>

                    <center><a href="Main.php">Tornar a la Agenda</a></center>
                    <?php
                }
                ?>


            </div>
        </div>
    </body>
</html>
