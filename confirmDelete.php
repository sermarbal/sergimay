<!DOCTYPE html> 
<html>

    <head>
        <link rel="stylesheet" type="text/css" href="css/confirmDelete.css">
        <link rel="shortcut icon" href="img/logo.png"/>
        <?php require 'php/functions.php'; ?>
        <title>Confirm Delete</title>
    </head>
    <body>
        <?php
        session_start();
        security();
        if (!$_GET['delete']) {
            ?>
            <div id="principal">
                <div id="close">
                    <a href="Main.php">
                        <img src="img/popUpClose.png" alt="" height="27" width="27">
                    </a>
                </div>

                <div id="question">
                    Are you sure you want to delete this contact?
                </div>

                <div id="yes">
                    <a href="<?php echo $_SERVER['PHP_SELF'] . "?id=" . $_GET['id'] . "&delete=1";
            ?>">
                        <img src="img/check.png" height="44" width="44"/>
                    </a>
                </div>

                <div id="no">
                    <a href="<?php echo $_SERVER['PHP_SELF'] . "?id=" . $_GET['id'] . "&delete=2"; ?>">
                        <img src="img/cross.png" height="44" width="44"/>
                    </a>
                </div>
            </div>
            <?php
        } else if ($_GET['delete'] == 1) {
            // Create connection
            $conn = getConnection();
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            // sql to delete a record
            $sql = "DELETE FROM CONTACTS WHERE id=" . $_GET['id'];

            if ($conn->query($sql) === TRUE) {
                echo "Record deleted successfully";
            } else {
                echo "Error deleting record: " . $conn->error;
            }
            ?>
        <enter><a href="Main.php">Tornar a la Agenda</a></center>
        <?php
        $conn->close();
    } else {
        header("Location: Main.php");
    }
    ?>

</body>

</html>