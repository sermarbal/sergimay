<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/addContact.css">
        <link rel="shortcut icon" href="img/logo.png"/>
        <script type="text/javascript" src="js/functions.js"></script>
        <?php require 'php/functions.php'; ?>
        <title>Edit Contact</title>
    </head>
    <body>

        <?php
        session_start();
        security();
        if (!$_POST) {
            ?>
            <div id = "principal">
                <div id = "close">
                    <a href="Main.php">
                        <img src = "img/popUpClose.png" alt = "" height = "38" width = "38">
                    </a>

                </div>

                <div id="editContact">

                    <h1>Contact</h1>
                    <img src="img/photoUsers.png" height="114" width="131">
                    <br>
                    <form action="<?php echo $_SERVER['PHP_SELF'] ?>?&id=<?php echo $_GET['id'] ?>" method="post">

                        <?php
                        // Create connection
                        $conn = getConnection();
                        // Check connection
                        if ($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }
                        $sql = "SELECT * FROM CONTACTS where id=" . $_GET['id'];
                        $result = mysqli_query($conn, $sql);
                        if (mysqli_num_rows($result) > 0) {
                            while ($row = mysqli_fetch_assoc($result)) {
                                ?>
                                <label> Name:</label>
                                <input type="text" name="name" value="<?php echo $row['name'] ?>" class="info">
                                <br>
                                <label>Surname:</label>
                                <input type="text" name="surname" value="<?php echo $row['surname'] ?>" class="info">
                                <br>
                                <label>Telephone:</label>
                                <input type="tel" name="telephone" value="<?php echo $row['telephone'] ?>" class="info">
                                <br>
                                <label>Birthday:</label>
                                <input id="dateInput" onclick="showCalendarEditContact()" type="date" name="birthday" value="<?php echo $row['birthday'] ?>" class="info">
                                <br>
                                <div align="center">
                                    <iframe id="iframe" src="js/calendar.html" frameborder="0" scrolling="no" onload="resizeIframe(this)" >
                                    </iframe>
                                </div>
                                <label>Email:</label>
                                <input type="email" name="email" value="<?php echo $row['email'] ?>" class="info">
                                <br>
                                <input type="submit" value="Save" class="button">
                                <div id="divider">

                                    <input type="reset" value="Reset" class="button">

                                    <?php if (isset($_COOKIE['error'])) { ?>
                                        <script type="text/javascript">
                                            error(<?php echo $_COOKIE['error'] ?>);
                                        </script>
                                        <?php
                                        setcookie('error');
                                    }
                                }
                            }
                            ?>
                    </form>
                </div>
            </div>
            <?php
        } else {
            if ($_POST['name'] == null) {
                setcookie("error", 0);
                header('Location: ' . $_SERVER['PHP_SELF'] . "?id=" . $_GET['id']);
            } else if ($_POST['telephone'] == null) {
                setcookie("error", 1);
                header('Location: ' . $_SERVER['PHP_SELF'] . "?id=" . $_GET['id']);
            } else if ($_POST['birthday'] == null) {
                setcookie("error", 2);
                header('Location: ' . $_SERVER['PHP_SELF'] . "?id=" . $_GET['id']);
            } else if ($_POST['email'] == null) {
                setcookie("error", 3);
                header('Location: ' . $_SERVER['PHP_SELF'] . "?id=" . $_GET['id']);
            } else {

                // Create connection
                $conn = getConnection();
                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                if ($_POST['name'] != null) {
                    $sql = "UPDATE CONTACTS SET name='" . $_POST['name'] . "' WHERE id=" . $_GET['id'];
                    if ($conn->query($sql) === TRUE) {
                        echo "Record updated successfully";
                    } else {
                        echo "Error updating record: " . $conn->error;
                    }
                }
                if ($_POST['surname'] != null) {
                    $sql = "UPDATE CONTACTS SET surname='" . $_POST['surname'] . "' WHERE id=" . $_GET['id'];
                    if ($conn->query($sql) === TRUE) {
                        echo "Record updated successfully";
                    } else {
                        echo "Error updating record: " . $conn->error;
                    }
                }
                if ($_POST['telephone'] != null) {
                    $sql = "UPDATE CONTACTS SET telephone='" . $_POST['telephone'] . "' WHERE id=" . $_GET['id'];
                    if ($conn->query($sql) === TRUE) {
                        echo "Record updated successfully";
                    } else {
                        echo "Error updating record: " . $conn->error;
                    }
                }
                if ($_POST['birthday'] != null) {
                    $sql = "UPDATE CONTACTS SET birthday='" . $_POST['birthday'] . "' WHERE id=" . $_GET['id'];
                    if ($conn->query($sql) === TRUE) {
                        echo "Record updated successfully";
                    } else {
                        echo "Error updating record: " . $conn->error;
                    }
                }
                if ($_POST['email'] != null) {
                    $sql = "UPDATE CONTACTS SET email='" . $_POST['email'] . "' WHERE id=" . $_GET['id'];
                    if ($conn->query($sql) === TRUE) {
                        echo "Record updated successfully";
                    } else {
                        echo "Error updating record: " . $conn->error;
                    }
                }

                $conn->close();
                ?>

            <center><a href="Main.php">Tornar a la Agenda</a></center>
            <?php
        }
    }
    ?>
</body>
</html>

