
function errorLogin(n) {
    if (n === 0)
        alert("Connection error.");
    else if (n === 1)
        alert("Invalid username.");
    else if (n === 2)
        alert("Invalid password.");
}

function errorContact(op) {

    if (op == 0)
        alert("ERROR: The name is necessary");
    else if (op == 1)
        alert("ERROR: The telephone is necessary");
    else if (op == 2)
        alert("ERROR: The birthday is necessary");
    else if (op == 3)
        alert("ERROR: The email is necessary");
}

function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    obj.style.width = obj.contentWindow.document.body.scrollWidth + 'px';
}

function showCalendar(obj) {

    if (document.getElementById("iframe").style.display == "none") {
        document.getElementById("iframe").style.display = "inline";
        document.getElementsByClassName("button")[0].style.left = "250px";
        document.getElementsByClassName("button")[1].style.left = "250px";
    } else {
        document.getElementById("iframe").style.display = "none";
        document.getElementsByClassName("button")[0].style.left = "110px";
        document.getElementsByClassName("button")[1].style.left = "110px";
    }

}

function showCalendarEditContact(obj) {

    if (document.getElementById("iframe").style.display == "none") {
        document.getElementById("iframe").style.display = "inline";
        document.getElementsByClassName("button")[0].style.left = "250px";
        document.getElementsByClassName("button")[1].style.left = "250px";
    } else {
        document.getElementById("iframe").style.display = "none";
        document.getElementsByClassName("button")[0].style.left = "250px";
        document.getElementsByClassName("button")[1].style.left = "250px";
    }

}



function updateOrder(op) {
    var options = document.getElementsByClassName("order");

    for (var i = 0; i < options.length; i++)
        if (options[i].getAttribute("value") == op)
            options[i].setAttribute("selected", "selected");
}

function updateFind(find, findby) {
    
    var options = document.getElementsByClassName("findby");

    document.getElementById("find").setAttribute("placeholder", find);

    
    for (var i = 0; i < options.length; i++)
        if (options[i].getAttribute("value") == findby) {
            options[i].setAttribute("selected", "selected");
            break;
        }
}

/*
function findPlaceHolder() {

    var findField = document.getElementById("find");

    if (findField.value == "")
        findField.value = findField.getAttribute("placeholder");

}
*/