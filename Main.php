<!DOCTYPE HTML>
<html>
    <meta charset="UTF-8">

    <head>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="shortcut icon" href="img/logo.png"/> 
        <script type="text/javascript" src="js/functions.js"></script>
        <?php require 'php/functions.php'; ?>
        <title>Agenda Online</title>
    </head>

    <?php
    session_start();
    security();
    if (isset($_GET['page'])) // && $_GET['page']>1
        $_GET['page'] = $_GET['page'];
    else
        $_GET['page'] = 1;
    ?>

    <body>
        <div id="principal">

            <h1>Agenda Online</h1>
            <div id="menuTop">
                <img href="mainUser.php" src="img/userLogo.png" alt="" height="44" width="44">
                <a href="logout.php">
                    <img src="img/logout.png" alt="" height="44" width="44">
                </a>
            </div>
            <a href="addContact.php" class="noUnderline">
                <div id="addContact">

                    <img src="img/addContact.png" alt="" height="33" width="32"> 

                    <span class="textAdd">Add Contact</span>
                </div>
            </a>

            <span class="displayUser"><?php echo $_SESSION['username'] ?></span>


            <form id="form" action="<?php echo $_SERVER['PHP_SELF']."?page=".$_GET['page'] ?>" method="POST">

                Find: <input id="find" type="text" name="find" placeholder="Find..."/>
                Find by: <select name="findby">
                    <option class="findby" value="all">All</option>
                    <option class="findby" value="name">name</option>
                    <option class="findby" value="surname">surname</option>
                    <option class="findby" value="telephone">telephone</option>
                    <option class="findby" value="birthday">birthday</option>
                </select>
                <br>
                Order: <select name="order">
                    <option class="order" value="orderby">Order by</option>
                    <option class="order" value="name">name</option>
                    <option class="order" value="surname">surname</option>
                    <option class="order" value="telephone">telephone</option>
                    <option class="order" value="birthday">birthday</option>
                </select>
                <input type="hidden" value="currentPage">
                <input type="hidden" value="totalPages">
                <input type="submit" name="submit" value="Submit"/><!--onclick="findPlaceHolder()"-->

            </form>
            <br>
            <?php if (isset($_POST['order'])) { ?>
                <script type="text/javascript">
                    updateOrder("<?php echo $_POST['order'] ?>");
                    updateFind("<?php echo $_POST['find'] ?>", "<?php echo $_POST['findby'] ?>");
                </script>
            <?php } ?>

            <div id="table-wrapper">
                <div id="table-scroll">
                    <table>
                        <thead>
                            <tr>
                                <th>Surname</th>
                                <th>Name</th>
                                <th>Telephone</th>
                                <th>Birthday</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                        <thead>

                            <?php
                            $conn = getConnection();

                            // Check connection
                            if (!$conn) {
                                $_SESSION['error'] = 0;
                                die("Connection failed: " . mysqli_connect_error());
                            } else {
                                $order = "";
                                $find = "";
                                $pagination = ""; // SELECT * FROM CONTACTS LIMIT 5, 10;
                                // Order
                                if ($_POST['order'] && strcmp($_POST['order'], "orderby")) {
                                    $order = "ORDER BY " . $_POST['order'] . "";
                                }

                                // Find
                                if ($_POST['find'] && $_POST['findby']) {
                                    switch ($_POST['findby']) {
                                        case "name":
                                        case "surname":
                                        case "birthday":
                                        case "telephone":
                                            $find = "AND " . $_POST['findby'] . " like '" . $_POST['find'] . "%'";
                                            break;
                                        case "all":
                                            $find = "AND name like '" . $_POST['find'] . "%' "
                                                    . "|| surname like '" . $_POST['find'] . "%' "
                                                    . "|| birthday like '" . $_POST['find'] . "%' "
                                                    . "|| telephone like '" . $_POST['find'] . "%'";
                                            break;
                                    }
                                }
                                
                                // Pagination
                                $endLimit = 5;
                                if($_GET['page']!=1)
                                    $startLimit = ($_GET['page'] * $endLimit)-$endLimit;
                                else
                                    $startLimit = "0";
                                
                                $pagination = "LIMIT " . $startLimit . "," . $endLimit;
                                
                                // Query 
                                $sql = "SELECT * FROM CONTACTS WHERE id_user=" . $_SESSION['id'] . " " . $find . " " . $order." ".$pagination;
                                // echo $sql;
                                $result = mysqli_query($conn, $sql);
                                if (mysqli_num_rows($result) > 0) {
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        ?>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $row['surname'] ?></td>
                                            <td><?php echo $row['name'] ?></td>
                                            <td><?php echo $row['telephone'] ?></td>
                                            <td><?php echo $row['birthday'] ?></td>
                                            <td><?php echo $row['email'] ?></td>
                                            <td><a href = "confirmDelete.php?id=<?php echo $row['id'] ?>"><img src = "img/delete.png" alt = "" height = "37" width = "37"></a>
                                                <a href = "editContact.php?id=<?php echo $row['id'] ?>"><img src = "img/edit.png" alt = "" height = "37" width = "37"></a></td>
                                        </tr>
                                    </tbody>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </table>
                    <!-- Prev Page -->
                    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="GET">
                        <input type="hidden" name="page" value="<?php 
                        if($_GET['page']>1) 
                            echo $_GET['page']-1;
                        else
                            echo $_GET['page'];?>">
                        <input type="submit" value="<" class="button">
                    </form>

                    <!-- Next Page -->
                    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="GET">
                        <input type="hidden" name="page" value="<?php echo $_GET['page']+1 ?>">
                        <input type="submit" value=">" class="button">
                    </form>


                </div>
            </div>
        </div>
    </div>
</body>

</html>

