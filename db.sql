/* a14sermarbal_a1 */

DROP DATABASE IF EXISTS a14sermarbal_A1;

CREATE DATABASE IF NOT EXISTS a14sermarbal_A1;

USE a14sermarbal_A1;

CREATE TABLE IF NOT EXISTS USERS(
    id int not null auto_increment PRIMARY KEY,
    username varchar(30) not null UNIQUE,
    name varchar(30) not null,
    surname varchar(30) not null,
    password varchar(60) not null,
    telephone varchar(15),
    birthday date,
    email varchar(60) not null,
    address varchar(60)
);

INSERT INTO USERS ( username, name, surname, password,telephone, birthday, email, address) 
VALUES ( "Sergi", "name1", "surname1", MD5("123456"), "65123456", "1992-7-24", "email1@mail.com", "address1");

INSERT INTO USERS ( username, name, surname, password,telephone, birthday, email, address) 
VALUES ( "May", "name2", "surname2", MD5("123456"), "65123457", "1992-12-13", "email2@mail.com", "address2");

CREATE TABLE IF NOT EXISTS CONTACTS(
    id int not null auto_increment PRIMARY KEY,
    surname varchar(30),
    name varchar(30) not null,
    telephone varchar(30) not null,
    birthday varchar(30) not null,
    email varchar(30),
    id_user int not null,
    FOREIGN KEY (id_user) REFERENCES USERS(id)
);

/* Sergi contacts */
INSERT INTO CONTACTS (name, telephone, birthday, email, id_user)
VALUES ( "Andrea", "654021365", "1994-05-13", "andrea@mail.com","1");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Alejandro", "Magno", "654123365", "1994-05-15", "aleMagno@mail.com", "1");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Julio", "Cesar", "654124325", "1994-05-18", "juliocesar@iam.cat", "1");

INSERT INTO CONTACTS (name, telephone, birthday, email, id_user)
VALUES ( "Julio", "654021365", "1994-05-13", "andrea@mail.com","1");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Manuel", "Ramirez", "654123365", "1994-05-15", "aleMagno@mail.com", "1");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Pepito", "Grillo", "654124325", "1994-05-18", "juliocesar@iam.cat", "1");

INSERT INTO CONTACTS (name, telephone, birthday, email, id_user)
VALUES ( "Pitagoras", "654021365", "1994-05-13", "andrea@mail.com","1");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Clark", "Kent", "654123365", "1994-05-15", "aleMagno@mail.com", "1");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Julio", "Berne", "654124325", "1994-05-18", "juliocesar@iam.cat", "1");

INSERT INTO CONTACTS (name, telephone, birthday, email, id_user)
VALUES ( "San Adria", "654021365", "1994-05-13", "andrea@mail.com","1");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Ermengol", "Bota", "654123365", "1994-05-15", "aleMagno@mail.com", "1");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Salvador", "Campo", "654124325", "1994-05-18", "juliocesar@iam.cat", "1");

/* May contacts */
INSERT INTO CONTACTS (name, telephone, birthday, email, id_user)
VALUES ( "Marc", "654021365", "1994-05-13", "andrea@mail.com","2");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Anna", "Magno", "654123365", "1994-05-15", "aleMagno@mail.com", "2");

INSERT INTO CONTACTS (name, surname, telephone, birthday, email, id_user)
VALUES ( "Mario", "Cesar", "654124325", "1994-05-18", "juliocesar@iam.cat", "2");