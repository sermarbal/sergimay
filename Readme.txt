1- Creació database.
2- Explicació Login.php
3- Explicació Main.php
4- Explicació AddContact.php
5- Explicació EditContact.php
6- Explicació remove contact
7- Explicació Logout.php
8- Explicació notLogued.php
9- Pasos previs.
10- Usuaris d'exemple per a connectar-se
11- Aspectes generals

1.
    
    Per crear la database utilitzarem el següent comandament "mysql -u root -p < db.sql" (s'ha de tenir en compte que falta canviar l'usuari amb el que entrem si s'utilitza des del labs).
    Un cop introduida tindrem la database creada i preparada. No ens ha de preocupar que estigui creada previament amb una versió anterior, l'escript borra la db si crea una de nova.

2.
    Login.php comprova la taula d'usuaris i verifica que l'usuari i password estiguin correctament introduits. Si no ho estàn mostra l'error amb un alert recarregan la pàgina.

3.
    Main.php revisa les dades de la taula dels contactes i recull tots els contactes que tinguin el identificador de l'usuari loguejat (variable $_SESSION['id'])
    Els botons de modificar i eliminar pasa el identificador del contacte en qüestió per $_GET.

4.
    Addcontact.php mostra un formulari on es poden introduir els camps i a partir d'aquests crear un contacte nou amb l'identificado d'usuari loguejat.
    Hi ha un camp opcional que està controlat per a que no doni cap problema si no s'omple.

5.
    EditContact.php Msotra un formulari amb les dades del contacte en qüestió. Dels camps opcionals es revisa que no estiguin null i actualiza les dades del contacte.

6.
    RemoveContact.php anava a ser un pop-up que demanava una confirmació per a que l'usuari confirmes si volia eliminar el contacte en qüestió però alfinal s'ha decidit fer-lo nomès com una pàgina més.

7.
    Logout.php com el seu nom indica deslogueja l'usuari i li mostra un link amb la opció de tornar a login.php (aquest link està retocat per a que vagi a la carpeta arrel del projecte ja que serà més útil tant per desenvolupament com per a validació i testeig).

8. 
    notLogued.php serà la pàgina on es redirecciona a un usuari no loguejat o incorrectament loguejat si es dona el cas. Es similar a logout però amb un missatge diferent.

9.  IMPORTANT
    
    - Per al bon funcionament del projecte primer de tot s'haurà d'introduir al servidor mysql la DB.
        Per a fer-ho utilitzarem la comanda mysql -u root -p < db.sql
    - Si estem al labs farà falta esborrar o moure el fitxer .htaccess que per algún motiu dona problemes.
    - Respecte a la conexió no farà falta res més degut a que la funció getConnection dins de php/functions.php mira el domini on estem i fa servir unes credencials o unes altres.
    - Tot i així si hi ha cap problema s'haura de mirar les credencials que s'utilitzen a la funció i canviar les parts necessàries.
        Per a canviar les credencials anem a l'arxiu functions.php i canviem l'usuari i contrasenya de la clausula else de la línia 14.
        Un cop fet això s'hauria de poder obrir sense problemesla connexió amb la DB i fer les gestions pertinents configurades a la web.

10.

    Pel al testeig de la pàgina web realitzada s'han facilitat els següents usuaris amb els que es podrà testejar la pàgina.

        user: sergi
        pass: 123456

        user: may
        pass: 123456

11.

    Sobre la pàgina:

        - S'utilitzarà $_SESSION['id'] per a verificar que l'usuari està loguejat correctament. A cada començament es crida a la funció security() per a verificar que l'usuari està loguejat correctament. Si no ho està el reenvia a notlogued.php que es simialr a logout.php.
        - No hi ha comprovació de que les dades introduides siguin correctes (pot haver-hi "hola mundo" al telèfon). L'unica que esta "comprovada" es el birthday ja que es un camp expressament per dates.
        - L'arxiu .htaccess ha sigut deshabilitat per donar problemes en el mostreig de la pàgina web. (htaccessOLD)

    Sobre la BD:

        - Es una relació simple 1-n entre dues taules (USERS 1 - n CONTACTS).
        - Les contrasenyes dels usuaris de la BD estàn guardades amb encriptació md5.