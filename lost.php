<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/lost.css">
        <link rel="stylesheet" type="text/css" href="css/general.css">
        <link rel="shortcut icon" href="img/logo.png"/>
        <title>Password Recovery</title>
    </head>
    <body>
        <div id="principal">
            <h1>Password Recovery</h1>

            <form>
                <img src="img/email.png" height="42" width="42">
                <input type="email" name="email" value="Email" size="30">
            </form>
            <br><br>
            <a href="">Send</a>
            <br><br>
            <div>
                <h2>Description</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <span>Curabitur convallis placerat posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel eleifend urna, id feugiat quam. Vestibulum ultricies tortor non magna tincidunt rutrum. Mauris efficitur, lectus eget tempus finibus, erat sem pulvinar lorem, et viverra lorem urna eget mauris. Nulla facilisi. Maecenas pharetra tortor eget tellus fermentum, nec semper est sollicitudin. Nulla nec congue lectus. Fusce feugiat urna vel pretium placerat.</span></p>
                <button type="button">view more</button> 
            </div>
        </div>
    </body>

</html>
