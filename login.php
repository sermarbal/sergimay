<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <script type="text/javascript" src="js/functions.js"></script>
        <?php require 'php/functions.php'; ?>
        <link rel="stylesheet" type="text/css" href="css/login.css">
        <link rel="shortcut icon" href="img/logo.png"/>
        <title>Login</title>
    </head>

    <?php
    if (!$_POST) {
        ?>
        <body>
            <div id="principal">
                <div>
                    <h1>Agenda Online</h1>
                    <form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
                        <div id="username">
                            <input id="user" type="text" name="username" value="Username" class="infoUser"><br>
                            <img src="img/userLogo.png" alt="" height="57" width="57"/>
                        </div>

                        <div id="password">
                            <input id="pass" type="password" name="password" value="Password" class="infoUser">
                            <br><br>
                            <img src="img/passwordLogo.png" alt="" height="61" width="61">
                        </div>
                        <input type="submit" value="Login" class="enter">
                    </form>
                </div>

                <div id="RegLost">
                    <a href="lost.php">Forgot password...</a>
                    <div id="divider"></div>
                    <a href="register.php">Register</a>
                </div>


                <div class="description">
                    <input type="checkbox" class="read-more-state" id="post-1" />
                    <h3>Description</h3>
                    <p class="read-more-wrap">Agenda Online keeps you connected to your friends in an easier and faster way. This fantastic app is used from all over the globe. Here are the functions:
                        <span class="read-more-target">It has a really secured database, private infos are safe. Validation of username and password to enter.It filters contacts by their name and surname.User friendly, easy to add a contact. You can also edit contact infos or delete them.                                     
                        </span></p>

                    <label for="post-1" class="read-more-trigger"></label>
                </div>

                <!-- int 0 = connection error 1 = error username 2 = password.-->
                <?php if (isset($_COOKIE['error'])) { ?>
                    <script type="text/javascript">
                        errorLogin(<?php echo $_COOKIE['error'] ?>);
                    </script>
                    <?php
                    setcookie('error');
                }
                ?>
            </div>

        </body>

        <?php
    } else {

        $conn = getConnection();

        // Check connection
        if (!$conn) {
            setcookie("error", 0);
            header('Location: ' . $_SERVER['PHP_SELF']);
            //die("Connection failed: " . mysqli_connect_error());
        } else {
            $sql = "SELECT id, username, password FROM USERS where username = \"" . $_POST['username'] . "\"";
            $result = mysqli_query($conn, $sql);

            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    echo "username: " . $row["username"] . " - password: " . $row["password"] . "<br>";

                    if (strcmp($row["password"], md5($_POST['password'])) != 0) {
                        setcookie("error", 2);
                        header('Location: ' . $_SERVER['PHP_SELF']);
                    } else {
                        session_start();
                        $_SESSION['id'] = $row["id"];
                        $_SESSION['username'] = $row["username"];
                        header('Location: ' . "Main.php");
                    }
                }
            } else {
                setcookie("error", 1);
                header('Location: ' . $_SERVER['PHP_SELF']);
            }
        }
    }
    ?>

</html>
